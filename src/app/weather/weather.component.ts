import { Component, OnInit } from '@angular/core';
import {IGeoResponse, IWeatherResponse, WeatherService} from './weather.service';
import {switchMap} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {faCity, faTemperatureHigh} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  faCity = faCity;
  faTemp = faTemperatureHigh;

  showWeatherDetails = false;
  showGeoDetails = false;

  weather: IWeatherResponse | null = null;
  geo: IGeoResponse | null = null;
  ready: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private readonly weatherService: WeatherService) { }

  ngOnInit(): void {
    this.weatherService.getGeo().pipe(
      switchMap((geo: IGeoResponse) => {
        this.geo = geo;
        return this.weatherService.getWeather(geo.latitude, geo.longitude);
      })
    ).subscribe((weather: IWeatherResponse) => {
      this.weather = weather;
      this.ready.next(true);
    });
  }

}
