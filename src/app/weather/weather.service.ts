import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

const FORECAST_URI = 'https://api.openweathermap.org/data/2.5/weather';

// Yes, these API keys maybe shouldn't be here.  Unauthenticated sites probably expose
// a backend API of their own that proxies requests to the real service anyway (maybe with caching).
const API_KEY = '35d0922b2ef66f9844b4f77032c3855e';
const GEO_URI = 'https://api.ipgeolocation.io/ipgeo?apiKey=2ab67584f9024d42a1f0131c57f1886d';

export interface IGeoResponse {
  ip: string;
  type: string;
  continent_code: string;
  continent_name: string;
  country_code2: string;
  country_name: string;
  state_prov: string;
  region_code: string;
  city: string;
  zipcode: string;
  latitude: number;
  longitude: number;
}

export interface IWeatherResponse {
  coord: {
    lon: number;
    lat: number;
  };
  weather: {
    id: number;
    main: string;
    description: string;
    icon: string;
  }[];
  base: string;
  main: {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max; number;
    pressure: number;
    humidity: number;
  };
  visibility: number;
  wind: {
    speed: number;
    deg: number;
    gust: number;
  };
  clouds: {
    all: number;
  };
  dt: number;
  sys: {
    type: number;
    id: number;
    country: string;
    sunrise: number;
    sunset: number;
  };
  timezone: number;
  id: number;
  name: string;
  cod: number;
}

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private readonly http: HttpClient) { }

  getWeather(latitude: number, longitude: number): Observable<IWeatherResponse> {
    const uri = `${FORECAST_URI}?appid=${API_KEY}&lat=${latitude}&lon=${longitude}&units=imperial`;
    return this.http.get<IWeatherResponse>(encodeURI(uri));
  }

  getGeo(): Observable<IGeoResponse> {
    return this.http.get<IGeoResponse>(GEO_URI);
  }
}
