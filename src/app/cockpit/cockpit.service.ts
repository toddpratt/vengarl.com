import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

export interface IResponse<T> {
  fields: {};
  entries: T[];
}

export interface ILink {
  title: string;
  href: string;
  description: string;
  published: string;
  imageSrc: string;
}

@Injectable({
  providedIn: 'root'
})
export class CockpitService {
  private readonly APIBASE = 'https://vengarl.com';
  private readonly TOKEN = '6f89e3929c98ccd0dcc441b9f9b34e';

  constructor(private readonly httpClient: HttpClient) { }

  getUrl(collectionName: string): string {
    return `${this.APIBASE}/cms/api/collections/get/${collectionName}?token=${this.TOKEN}`;
  }

  getLinks(): Observable<IResponse<ILink>> {
    const url = this.getUrl('Links');
    return this.httpClient.get<IResponse<ILink>>(url);
  }
}
