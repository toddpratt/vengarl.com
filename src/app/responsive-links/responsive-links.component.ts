import {Component, OnInit} from '@angular/core';
import {CockpitService, ILink, IResponse} from '../cockpit/cockpit.service';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-responsive-links',
  templateUrl: './responsive-links.component.html',
  styleUrls: ['./responsive-links.component.css']
})
export class ResponsiveLinksComponent implements OnInit {
  ready: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  links: ILink[];

  constructor(private readonly cockpitService: CockpitService) { }

  ngOnInit(): void {
    this.cockpitService.getLinks().subscribe((response: IResponse<ILink>) => {
      this.links = response.entries;
      this.ready.next(true);
    });
  }
}
